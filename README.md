# Project self-provisioning

This project will contain the necessary manifests to prevent authenticated users creating namespaces under an Openshift 4 cluster.

Note that a user must request a project through the corresponding Web Frameworks UI.

* `self-provisioners.yaml`: this manifest will prevent authenticated users to self-provision new projects. It's important to note that `rbac.authorization.kubernetes.io/autoupdate` annotation must be set to `false` to prevent automatic updates to the role, hence resetting the cluster role to the default state.

* `request-project-message.yaml`: customized message to provide more helpful instructions to the users at the time of creating a new project.

## Links

* Official documentation: https://docs.openshift.com/container-platform/latest/applications/projects/configuring-project-creation.html#disabling-project-self-provisioning_configuring-project-creation